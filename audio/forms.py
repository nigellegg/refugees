from django import forms
from django.core.exceptions import ValidationError
from refugee.textarea import SmallTextArea

class audioFileForm(forms.Form):
	af_file = forms.FileField(
		label='Select an audiofile',
		help_text='Max. size 42 megabytes')
	af_desc = forms.CharField(
		widget=SmallTextArea(attrs={'class': 'resizeable'}),
		label='Description')
	af_tag1 = forms.CharField(
		label="tag 1:")
	af_tag2 = forms.CharField(
		label="tag 2:")
	af_tag3 = forms.CharField(
		label="tag 3:")
	af_tag4 = forms.CharField(
		label="tag 4:")
	af_cont = forms.CharField(
		label='Select continent of origin')
	af_shareable = forms.BooleanField(
		label='Can the story be shared offline')
	af_lang = forms.CharField(
		label='Language')
	af_namevis = forms.BooleanField(
		label="Show your name")

