from django.db import models
from django.contrib.auth.models import User

class audiofile(models.Model):
	ASIA = "AS"
	AFRICA = "AF"
	MIDDLEEAST = "ME"
	SOUTHAMERICA = "LA"
	AF_CONT_CHOICES = (
		(ASIA, "Asia"),
		(AFRICA, "Africa"),
		(MIDDLEEAST, "Middle East"),
		(SOUTHAMERICA, "South America"),
	)
	af_file = models.FileField()
	af_desc = models.CharField(max_length=200)
	af_tag1 = models.CharField(max_length=20)
	af_tag2 = models.CharField(max_length=20)
	af_tag3 = models.CharField(max_length=20)
	af_tag4 = models.CharField(max_length=20)
	af_user = models.ForeignKey(User)
	af_datetime = models.DateTimeField()
	af_cont = models.CharField(max_length=2, 
		                       choices=AF_CONT_CHOICES)
	af_shareable = models.BooleanField(default=False)
	af_lang = models.CharField(max_length=10)
	af_namevis = models.BooleanField(default=False)