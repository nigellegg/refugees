from django.shortcuts import render_to_response, render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.template import RequestContext, Context, loader
from django.contrib.auth.models import User
from audio.models import audiofile
from audio.forms import audioFileForm
import datetime 
import logging

log = logging.getLogger(__name__)

def upload(request):
    user = request.user
    log.debug(user.username)
    if request.method == 'POST':
        form = audioFileForm(request.POST, request.FILES)
        if form.is_valid():
            aud = audiofile(af_file=request.FILES['af_file'])
            aud.af_cont = form.cleaned_data['af_cont']
            aud.af_desc = form.cleaned_data['af_desc']
            aud.af_tag1 = form.cleaned_data['af_tag1']
            aud.af_tag2 = form.cleaned_data['af_tag2']
            aud.af_tag3 = form.cleaned_data['af_tag3']
            aud.af_tag4 = form.cleaned_data['af_tag4']
            aud.af_datetime = datetime.datetime.now()
            aud.af_user = user
            aud.af_shareable = form.cleaned_data['af_shareable']
            aud.af_lang = form.cleaned_data['af_lang']
            aud.save()
            return HttpResponseRedirect(reverse('audio.views.upload'))
    else:
        form = audioFileForm()
    return render(request, 'upload_audio.html', {'form': form})    