from django.db import models
from django.contrib.auth.models import User

class imagefile(models.Model):
	ASIA = "AS"
	AFRICA = "AF"
	MIDDLEEAST = "ME"
	SOUTHAMERICA = "LA"
	IF_CONT_CHOICES = (
		(ASIA, "Asia"),
		(AFRICA, "Africa"),
		(MIDDLEEAST, "Middle East"),
		(SOUTHAMERICA, "South America"),
	)
	if_file = models.FileField()
	if_desc = models.CharField(max_length=200)
	if_tag1 = models.CharField(max_length=20)
	if_tag2 = models.CharField(max_length=20)
	if_tag3 = models.CharField(max_length=20)
	if_tag4 = models.CharField(max_length=20)
	if_user = models.ForeignKey(User)
	if_datetime = models.DateTimeField()
	if_cont = models.CharField(max_length=2, 
		                       choices=IF_CONT_CHOICES)
	if_shareable = models.BooleanField(default=False)
	if_lang = models.CharField(max_length=10)
	if_namevis = models.BooleanField(default=False)