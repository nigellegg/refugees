from django.shortcuts import render_to_response, render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.template import RequestContext, Context, loader
from django.contrib.auth.models import User
from audio.models import imagefile
from audio.forms import imageFileForm
import datetime 
import logging

log = logging.getLogger(__name__)

def upload(request):
    user = request.user
    log.debug(user.username)
    if request.method == 'POST':
        form = imgeFileForm(request.POST, request.FILES)
        if form.is_valid():
            img = imagefile(if_file=request.FILES['if_file'])
            img.if_cont = form.cleaned_data['if_cont']
            img.if_desc = form.cleaned_data['if_desc']
            img.if_tag1 = form.cleaned_data['if_tag1']
            img.if_tag2 = form.cleaned_data['if_tag2']
            img.if_tag3 = form.cleaned_data['if_tag3']
            img.if_tag4 = form.cleaned_data['if_tag4']
            img.if_datetime = datetime.datetime.now()
            img.if_user = user
            img.if_shareable = form.cleaned_data['if_shareable']
            img.if_lang = form.cleaned_data['if_lang']
            img.save()
            return HttpResponseRedirect(reverse('image.views.upload'))
    else:
        form = audioFileForm()
    return render(request, 'upload_imaage.html', {'form': form})    