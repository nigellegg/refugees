from django.shortcuts import render_to_response, render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.template import RequestContext, Context, loader
from django.contrib.auth.models import User
from story.models import textstory, uptext
from story.forms import textstoryForm, uploadtextForm
import datetime
import json


def enter_text(request):
    user = request.user
    if request.method == 'POST':
        form = textstoryForm(request.POST)
        if form.is_valid():
            text = textstory()
            text.st_text = form.cleaned_data['st_text']
            text.st_desc = form.cleaned_data['st_desc']
            text.st_tag1 = form.cleaned_data['st_tag1']
            text.st_tag2 = form.cleaned_data['st_tag2']
            text.st_tag3 = form.cleaned_data['st_tag3']
            text.st_tag4 = form.cleaned_data['st_tag4']
            text.st_user = user
            text.st_datetime = datetime.datetime.now()
            text.st_shareable = form.cleaned_data['st_shareable']
            text.st_lang = form.cleaned_data['st_lang']
            if form.cleaned_data['st_namevis'] == True:
                text.st_namevis = form.cleaned_data['st_namevis']
            else: 
                text.st_namevis = False
            text.save()
            return HttpResponseRedirect(reverse('story.views.enter_text'))
    else:
        form = textstoryForm()
    return render(request, 'text_story.html', {'form': form})


def upload_text(request):
    user = request.user
    if request.method == 'POST':
        form = uploadtextForm(request.POST, request.FILES)
        if form.is_valid():
            text = uptext(ut_textfile=request.FILES['ut_textfile'])
            text.ut_desc = form.cleaned_data['ut_desc']
            text.ut_tag1 = form.cleaned_data['ut_tag1']
            text.ut_tag2 = form.cleaned_data['ut_tag2']
            text.ut_tag3 = form.cleaned_data['ut_tag3']
            text.ut_tag4 = form.cleaned_data['ut_tag4']
            text.ut_user = user
            text.ut_datetime = datetime.datetime.now()
            text.ut_shareable = form.cleaned_data['ut_shareable']
            text.ut_lang = form.cleaned_data['ut_lang']
            text.ut_namevis = form.cleaned_data['ut_namevis']
            text.save()
            return HttpResponseRedirect(reverse('story.views.upload_text'))
    else:
        form = uploadtextForm()
    return render(request, 'upload_text.html', {'form': form})

