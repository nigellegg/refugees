from django import forms
from django.core.exceptions import ValidationError
from refugee.textarea import SmallTextArea

class WideTextArea(forms.Textarea):
    def __init__(self, *args, **kwargs):
            kwargs.setdefault('attrs', {}).update({'rows': '20', 'cols': '120'})
            super(WideTextArea, self).__init__(*args, **kwargs)


class textstoryForm(forms.Form):
    st_text = forms.CharField(
        widget=WideTextArea(attrs={'class': 'resizable'}),
        label="Your Story", 
        help_text="Type or copy/paste your story here")
    st_desc = forms.CharField(
        widget=SmallTextArea(attrs={'class': 'resizeable'}),
        label='Description')
    st_tag1 = forms.CharField(
        label="tag 1:")
    st_tag2 = forms.CharField(
        label="tag 2:")
    st_tag3 = forms.CharField(
        label="tag 3:")
    st_tag4 = forms.CharField(
        label="tag 4:")
    st_cont = forms.CharField(
        label='Select continent of origin')
    st_shareable = forms.BooleanField(
        label='Can the story be shared offline')
    st_lang = forms.CharField(
        label='Language')
    st_namevis = forms.BooleanField(
        label="Show your name")

class uploadtextForm(forms.Form):
    ut_textfile = forms.FileField(
        label="Select a text file",
        help_text="max. size 42 megabytes")
    ut_desc = forms.CharField(
        widget=SmallTextArea(attrs={'class': 'resieable'}),
        label='Description')
    ut_tag1 = forms.CharField(
        label="tag 1")
    ut_tag2 = forms.CharField(
        label="tag 2")
    ut_tag3 = forms.CharField(
        label="tag 3")
    ut_tag4 = forms.CharField(
        label="tag 4")
    ut_cont = forms.CharField(
        label="Select continent of origin")
    ut_shareable = forms.BooleanField(
        label="Can the story be shared offline?")
    st_lang = forms.CharField(
        label="Language")
    st_namevis = forms.BooleanField(
        label="Show your name")
