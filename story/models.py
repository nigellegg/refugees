from django.db import models
from django.contrib.auth.models import User

class textstory(models.Model):
	ASIA = "AS"
	AFRICA = "AF"
	MIDDLEEAST = "ME"
	SOUTHAMERICA = "LA"
	AF_CONT_CHOICES = (
		(ASIA, "Asia"),
		(AFRICA, "Africa"),
		(MIDDLEEAST, "Middle East"),
		(SOUTHAMERICA, "South America"),
	)
	st_text = models.CharField(max_length=1000)
	st_desc = models.CharField(max_length=200)
	st_tag1 = models.CharField(max_length=20)
	st_tag2 = models.CharField(max_length=20)
	st_tag3 = models.CharField(max_length=20)
	st_tag4 = models.CharField(max_length=20)
	st_user = models.ForeignKey(User)
	st_datetime = models.DateTimeField()
	st_cont = models.CharField(max_length=2, 
		                       choices=AF_CONT_CHOICES)
	st_shareable = models.BooleanField(default=False)
	st_lang = models.CharField(max_length=10)
	st_namevis = models.BooleanField(default=False)


class uptext(models.Model):
    ut_textfile = models.FileField()
    ut_desc = models.CharField(max_length=200)
    ut_tag1 = models.CharField(max_length=20)
    ut_tag2 = models.CharField(max_length=20)
    ut_tag3 = models.CharField(max_length=20)
    ut_tag4 = models.CharField(max_length=20)
    ut_user = models.ForeignKey(User)
    ut_datetime = models.DateTimeField()
    ut_cont = models.CharField(max_length=20)
    ut_lang = models.CharField(max_length=10)
    ut_shareable = models.BooleanField(default=False)
    ut_namevis = models.BooleanField(default=False)

