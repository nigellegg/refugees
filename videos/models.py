from django.db import models
from django.contrib.auth.models import User

class upvideo(models.Model):
	ASIA = "AS"
	AFRICA = "AF"
	MIDDLEEAST = "ME"
	SOUTHAMERICA = "LA"
	VF_CONT_CHOICES = (
		(ASIA, "Asia"),
		(AFRICA, "Africa"),
		(MIDDLEEAST, "Middle East"),
		(SOUTHAMERICA, "South America"),
	)
	vf_file = models.FileField()
	vf_desc = models.CharField(max_length=200)
	vf_tag1 = models.CharField(max_length=20)
	vf_tag2 = models.CharField(max_length=20)
	vf_tag3 = models.CharField(max_length=20)
	vf_tag4 = models.CharField(max_length=20)
	vf_user = models.ForeignKey(User)
	vf_datetime = models.DateTimeField()
	vf_cont = models.CharField(max_length=2, 
		                       choices=VF_CONT_CHOICES)
	vf_shareable = models.BooleanField(default=False)
	vf_lang = models.CharField(max_length=10)
	vf_namevis = models.BooleanField(default=False)