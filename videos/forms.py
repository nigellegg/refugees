from django import forms
from django.core.exceptions import ValidationError
from refugee.textarea import SmallTextArea


class uploadvideoForm(forms.Form):
	vf_file = forms.FileField(
		label='Select an video file',
		help_text='Max. size 42 megabytes')
	vf_desc = forms.CharField(
		widget=SmallTextArea(attrs={'class': 'resizeable'}),
		label='Description')
	vf_tag1 = forms.CharField(
		label="tag 1:")
	vf_tag2 = forms.CharField(
		label="tag 2:")
	vf_tag3 = forms.CharField(
		label="tag 3:")
	vf_tag4 = forms.CharField(
		label="tag 4:")
	vf_cont = forms.CharField(
		label='Select continent of origin')
	vf_shareable = forms.BooleanField(
		label='Can the story be shared offline')
	vf_lang = forms.CharField(
		label='Language')
	vf_namevis = forms.BooleanField(
		label="Show your name")

