from django.shortcuts import render_to_response, render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.template import RequestContext, Context, loader
from django.contrib.auth.models import User
from videos.models import upvideo
from videos.forms import uploadvideoForm
import datetime


def upload_video(request):
    user = request.user
    if request.method == 'POST':
        form = uploadvideoForm(requet.POST, request.FILES)
        if form.is_valid():
            vid = upvideo(vf_file=request.FILES['vf_file'])
            vid.vf_desc = form.cleaned_data['vf_desc']
            vid.vf_tag1 = form.cleaned_data['vf_tag1']
            vid.vf_tag2 = form.cleaned_data['vf_tag2']
            vid.vf_tag3 = form.cleaned_data['vf_tag3']
            vid.vf_tag4 = form.cleaned_data['vf_tag4']
            vid.vf_user = user
            vid.vf_datetime = datetime.datetime.now()
            vid.vf_shareable = form.cleaned_data['vf_shareable']
            vid.vf_lang = form.cleaned_data['vf_lang']
            vid.vf_namevis = form.cleaned_data['vf_namevis']
            vid.save()
            return HttpResponseRedirect(reverse('videos.views.upload_video'))
    else:
        form = uploadtextForm()
    return render(request, 'upload_video.html', {'form': form})
