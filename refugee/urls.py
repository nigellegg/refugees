from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'fugee.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^index/', 'refugee.views.index', name='index'),
    #url(r'^search/', 'search.views.start', name='start'),
    url(r'^uptext/', 'story.views.upload_text', name='uptext'),
    url(r'^upaudio/', 'audio.views.upload', name='upaudio'),
    url(r'^upvideo/', 'videos.views.upload_video', name='upvideo'),
    url(r'^upimage/', 'image.views.upload', name='upimage'),
    url(r'^textstory/', 'story.views.enter_text', name='textstory'),
    url(r'^uptext/', 'story.views.upload_text', name='uptext'),
)
