from django import forms

class SmallTextArea(forms.Textarea):
    def __init__(self, *args, **kwargs):
            kwargs.setdefault('attrs', {}).update({'rows': '5', 'cols': '40'})
            super(SmallTextArea, self).__init__(*args, **kwargs)

